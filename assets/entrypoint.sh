#!/bin/sh

[ "${TRACE:-0}" -gt 0 ] && set -x

if ! whoami >/dev/null 2>&1 ; then
  if [ -w /etc/passwd ]; then
    echo "${USER_NAME:-default}:x:$(id -u):0:${USER_NAME:-default} user:${HOME}:/bin/bash" >> /etc/passwd
  fi
fi

if [ -n "$1" ] ; then
    exec /sbin/tini -- "$@"
else
    exec /bin/bash
fi
