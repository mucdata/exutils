FROM docker.io/golang:alpine AS go
RUN apk update && \
    apk upgrade && \
    apk --no-cache add upx binutils && \
    CGO_ENABLED=0 /usr/local/go/bin/go install github.com/nalbury/promql-cli@v0.3.0 && \
    strip /go/bin/promql-cli && \
    upx /go/bin/promql-cli && \
    /go/bin/promql-cli help

FROM docker.io/alpine:3.21.2
LABEL org.opencontainers.image.authors="DaTa"
RUN apk update && \
    apk upgrade && \
    apk --no-cache add \
    apache2-utils \
    bash \
    bash-completion \
    bind-tools \
    busybox \
    busybox-extras \
    coreutils \
    curl \
    git \
    httpie \
    iproute2 \
    jo \
    jq \
    netcat-openbsd \
    openssh \
    openssl \
    py3-yaml \
    py3-requests \
    py3-rich \
    py3-bottle \
    py3-aiohttp-retry \
    python3 \
    rsync \
    screen \
    socat \
    tinyproxy \
    tini \
    tmux \
    trurl \
    util-linux \
    vim \
    wget \
    && mkdir /user \
    && sed -i 's/^Allow/#Allow/' /etc/tinyproxy/tinyproxy.conf
COPY --from=go /go/bin/promql-cli /usr/local/bin/promql
COPY assets/bashrc /user/.bashrc
COPY assets/digrc /user/.digrc
COPY assets/entrypoint.sh /entrypoint.sh
RUN chown -R root:root /user && chmod -R g+w /user && chmod g=u /etc/passwd && \
    awk -F: '{print $3}' < /etc/passwd | sort > /tmp/foo-existing && \
    seq 65534 | sort > /tmp/foo-wanted && \
    comm -23 /tmp/foo-wanted /tmp/foo-existing | sort -n > /tmp/foo-missing && \
    awk '{ print "user" $0 ":x:" $0 ":0:user " $0 ":/user:/bin/bash" }' /tmp/foo-missing >> /etc/passwd && \
    rm -f /tmp/foo-*
ENV HOME /user
ENV SHELL /bin/bash
WORKDIR /user
ENTRYPOINT ["/entrypoint.sh"]
